
alias dc=docker-compose

docker login oci.tuxm.art:8443 --username ${HARBOR_USER} --password ${HARBOR_PASS}

dc down
dc pull
dc up -d

